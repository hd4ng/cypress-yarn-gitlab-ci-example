// enables intelligent code completion for Cypress commands
// https://on.cypress.io/intelligent-code-completion
/// <reference types="Cypress" />

it('works', function () {
  cy.wrap('foo').should('equal', 'foo')
})
